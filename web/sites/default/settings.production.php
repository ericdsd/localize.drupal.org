<?php

$config['system.logging']['error_level'] = 'hide';

// This will prevent Drupal from setting read-only permissions on sites/default.
$settings['skip_permissions_hardening'] = TRUE;

// This will ensure the site can only be accessed through the intended host
// names. Additional host patterns can be added for custom configurations.
$settings['trusted_host_patterns'] = ['.*'];

// Don't use Symfony's APCLoader. ddev includes APCu; Composer's APCu loader has
// better performance.
$settings['class_loader_auto_detect'] = FALSE;

$config['drupalorg']['site_status'] = '';

// SSO is always enabled on production.
$config['drupalorg_crosssite.settings']['disable_sso'] = FALSE;

$settings['config_readonly'] = TRUE;

// Configuration active only on live instance, e. g. most languages (too many
// languages on a dev environment dramatically slows down installation).
$config['config_split.config_split.live']['status'] = TRUE;
