<?php

$databases['default']['default'] = [
  'database' => getenv('DB_NAME'),
  'username' => getenv('DB_USERNAME'),
  'password' => getenv('DB_PASSWORD'),
  'host' => getenv('DB_HOST'),
  'port' => '3306',
  'driver' => 'mysql',
  'charset' => 'utf8mb4',
  'collation' => 'utf8mb4_general_ci',
  'prefix' => '',
];
$databases['migrate']['default'] = [
  'database' => 'migrate',
  'username' => getenv('DB_USERNAME'),
  'password' => getenv('DB_PASSWORD'),
  'host' => getenv('DB_HOST'),
  'port' => '3306',
  'driver' => 'mysql',
  'charset' => 'utf8mb4',
  'collation' => 'utf8mb4_general_ci',
  'prefix' => '',
];

$settings['hash_salt'] = getenv('DRUPAL_HASH_SALT');
$settings['config_sync_directory'] = '../config/sync';

$settings['config_exclude_modules'] = [
  'devel_generate',
  'stage_file_proxy',
  'config_inspector',
  'devel',
  'migrate_devel',
  'upgrade_status',
  'upgrade_rector',
];
$config['drupalorg']['site_status'] = 'This is a non-production site';
$config['drupalorg']['drupalorg_site'] = 'localize';

// Drupalorg Cross-site.
$config['drupalorg_crosssite.settings']['sub_site'] = TRUE;

// SSO settings for all sites except the ones that override them via their
// specific settings files.
$config['social_auth_keycloak.settings']['server_url'] = getenv('SSO_SERVER_URL');
$config['social_auth_keycloak.settings']['client_id'] = getenv('SSO_CLIENT_ID');
$config['social_auth_keycloak.settings']['client_secret'] = getenv('SSO_CLIENT_SECRET');
