#!/usr/bin/env bash

# Begin with a status overview.
ddev drush migrate:status --group=base,group,l10n

# Migrate base.
ddev drush migrate:import l10n_server_language
ddev drush migrate:import localize_d7_file
ddev drush migrate:import localize_d7_user
ddev drush migrate:import localize_d7_taxonomy_term_vocabulary_news_tags
ddev drush migrate:import localize_d7_node_complete_news
ddev drush migrate:import localize_d7_node_complete_page

# Migrate group.
ddev drush migrate:import l10n_server_group
ddev drush migrate:import localize_d7_node_complete_poll
ddev drush migrate:import localize_d7_taxonomy_term_vocabulary_group_tags
ddev drush migrate:import localize_d7_node_complete_story
ddev drush migrate:import localize_d7_node_complete_wiki

# Migrate localization tables.
ddev drush migrate:import l10n_server_project
ddev drush migrate:import l10n_server_release
ddev drush migrate:import l10n_server_file
ddev drush migrate:import l10n_server_line
ddev drush migrate:import l10n_server_string
ddev drush migrate:import l10n_server_status_flag
ddev drush migrate:import l10n_server_error
ddev drush migrate:import l10n_server_translation
ddev drush migrate:import l10n_server_translation_history
